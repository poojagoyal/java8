package com.main;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import com.model.Trader;
import com.model.Transaction;

public class MainClass {

	public static void main(String[] args) {

		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario", "Milan");
		Trader alan = new Trader("Alan", "Cambridge");
		Trader brian = new Trader("Brian", "Cambridge");

		List<Transaction> transactions = Arrays.asList(new Transaction(brian, 2011, 300),
				new Transaction(raoul, 2012, 1000), new Transaction(raoul, 2011, 400),
				new Transaction(mario, 2012, 710), new Transaction(mario, 2012, 700), new Transaction(alan, 2012, 950));

		// all transactions in the year 2011 and sorted by value (small to high).
		List<Transaction> transaction1 = transactions.stream()
				.filter(transaction -> transaction.getYear() == 2011)
				.sorted(comparing(Transaction::getValue)).collect(toList());
		
		System.out.println(transaction1);

		// all the unique cities where the traders work
		List<String> cities = transactions.stream()
				.map(transaction -> transaction.getTrader().getCity())
				.distinct()
				.collect(toList());
		
		System.out.println(cities);

		// all traders from Cambridge and sort them by name

		List<Trader> traders = transactions.stream().map(Transaction::getTrader).distinct()
				.filter(trader -> trader.getCity().equals("Cambridge"))
				.sorted(comparing(Trader::getName))
				.collect(toList());

		System.out.println(traders);

		// Return a string of all traders’ names sorted alphabetically.
		String names = transactions.stream()
				.map(transaction -> transaction.getTrader().getName())
				.distinct()
				.sorted()
				.reduce("", (name1, name2) -> name1 + name2);

		System.out.println(names);

		// Are any traders based in Milan?

		boolean flag = transactions.stream()
				.anyMatch(transaction -> transaction.getTrader().getCity().equals("Milan"));

		System.out.println(flag);

		// Print all transactions’ values from the traders living in Cambridge.
		transactions.stream()
				.filter(transaction -> transaction.getTrader().getCity().equals("Cambridge"))
				.map(Transaction::getValue).forEach(System.out::println);

		// What’s the highest value of all the transactions?

		Optional<Integer> highestValue = transactions.stream()
				.map(Transaction::getValue)
				.reduce(Integer::max);

		System.out.println(highestValue.get());

		// Find the transaction with the smallest value

		Optional<Transaction> lowestValueTransaction = transactions.stream()
				.min(comparing(Transaction::getValue));

		System.out.println(lowestValueTransaction.get());
	}

}
